package fr.upem.net.inet;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 22/01/14
 * Time: 09:09
 * To change this template use File | Settings | File Templates.
 */
public class AboutInetAddress {
    public static void main(String[] args) throws UnknownHostException {
        InetAddress myLocalHost = InetAddress.getLocalHost();
        System.out.println("My own \"local\" address is : " + myLocalHost);

        if (args.length == 0)
            return;

        String name = args[0];
        //
        InetAddress[] allByName = InetAddress.getAllByName(name);
        //InetAddress ia = InetAddress.getByName(name);
        for (InetAddress ia : allByName) {
            System.out.println("default 0 = " + ia);

            System.out.println("hostname = " + ia.getHostName());
            System.out.println("canonical =   " + ia.getCanonicalHostName());
            System.out.println("default 1 = " + ia);
        }
    }


}
