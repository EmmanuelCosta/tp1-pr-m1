package fr.upem.net.inet;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 22/01/14
 * Time: 11:22
 * To change this template use File | Settings | File Templates.
 */
public class EchoUDPServer {


    private final DatagramSocket datagramSocket;

    public EchoUDPServer(int port) throws SocketException {

        this.datagramSocket = new DatagramSocket(port);

    }

    public void launch() {
        byte[] buf = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        try {
            while (true) {


                datagramSocket.receive(packet);


                datagramSocket.send(packet);    //envoie le packet reponse
                packet.setData(buf, 0, buf.length);   //Remet la taille à jour

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //TAille maximale pour question d'efficacité
    //Autrement le serveur devrait calculée pour chaque paquet recu une taille appropriée
    //fixer la taille du buffer permet donc un gain de temps
    public static void main(String[] args) throws IOException {
        EchoUDPServer echoUDPServer = new EchoUDPServer(new Integer(args[0]));
        System.out.println("serveur launch");

        echoUDPServer.launch();

    }
}
