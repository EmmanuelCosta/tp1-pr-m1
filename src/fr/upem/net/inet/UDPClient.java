package fr.upem.net.inet;

import java.io.IOException;
import java.net.*;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 22/01/14
 * Time: 10:54
 * To change this template use File | Settings | File Templates.
 */
public class UDPClient {

    public static void main(String[] args) throws IOException {
            //Verification basique du nombre d'argument

        if(args.length < 3 )   {
            System.err.println("Not enough arguments");
            return;
        }
        DatagramSocket socket=new DatagramSocket();
        //Phase d'envoie
        //recuperation des parametres
        String serveur = args[0];
        String port=args[1];
        String msg = args[2];
        //buffer de stockage des reponses serveur
        byte[] buff= new byte[1024];


        System.out.println("socket local attachée à "+socket.getLocalAddress()+"/"+socket.getLocalPort());
        System.out.println(msg.length()+" octets emis vers "+serveur+"/"+port);
        System.out.println("capacité de la zone de stockage "+buff.length);

        //construction du paquet message
        byte[] buf = msg.getBytes();

        InetAddress ipServ= InetAddress.getByName(serveur);
        SocketAddress dest=new InetSocketAddress(serveur,new Integer(port));

        DatagramPacket packet=new DatagramPacket(buf,buf.length,dest);

        socket.send(packet);

          //RECEPTION

        packet.setData(buff);
        socket.receive(packet);

        System.out.println(packet.getLength()+" octects recus");
        String s=new String(buff, 0,packet.getLength(),"UTF8");
        System.out.println("contenant: "+s);

        System.out.println("provenant de : "+packet.getAddress()+"/"+packet.getPort());

        socket.close();

    }

}
