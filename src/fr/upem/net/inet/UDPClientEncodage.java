package fr.upem.net.inet;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 22/01/14
 * Time: 11:59
 * To change this template use File | Settings | File Templates.
 */
public class UDPClientEncodage {
    //FAIRE LA TRANSFORMATION
    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            System.err.println("Not enough arguments");
            return;
        }
        DatagramSocket socket = new DatagramSocket();
        //ENVOIE
        String serveur = args[0];
        String port = args[1];
       // String msg = args[2];
        String encoding = args[2];

        while (true) {
            System.out.println("veuillez entrer le mot à encoder");
            Scanner sc = new Scanner(System.in);
            String msg = sc.next();

            byte[] buff = new byte[1024];
            System.out.println("socket local attachée à" + socket.getLocalAddress() + "/" + socket.getLocalPort());
            System.out.println(msg.length() + " octets emis vers " + serveur + "/" + port);
            System.out.println("capacité de la zone de stockage " + buff.length);

            byte[] buf = msg.getBytes(encoding);

            InetAddress ipServ = InetAddress.getByName(serveur);
            SocketAddress dest = new InetSocketAddress(serveur, new Integer(port));

            DatagramPacket packet = new DatagramPacket(buf, buf.length, dest);

            socket.send(packet);

            //RECEPTION
            socket.setSoTimeout(2000);
            packet.setData(buff);
            try {
                socket.receive(packet);
                System.out.println(packet.getLength() + " recu");
                String s = new String(buff, 0, packet.getLength(), encoding);
                System.out.println("contenant " + s);

                System.out.println("provenant de : " + packet.getAddress() + "/" + packet.getPort());

            } catch (SocketTimeoutException e) {
                System.out.println("Le serveur n'a pas repondu");
            }
        }

    }
}
