package fr.upem.net.udp;

/**
 * Created with IntelliJ IDEA.
 * User: ebabalac
 * Date: 22/01/14
 * Time: 12:29
 * To change this template use File | Settings | File Templates.
 */


import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * This utility class offers conversion static methods between primitive types
 * and byte arrays for network transport.
 */
public class Converter {

    /**
     * Returns the long primitive value being represented by the argument byte
     * array in network order (big endian). The lowest index in the array
     * contains the most significant byte of the long value.
     *
     * @param array the byte array representing a long value in network order.
     * @return the long value represented by the byte array.
     * @throws IllegalArgumentException if the byte array size is incompatible
     *                                  with the long representation.
     */
    public static long byteArrayToLong(byte[] array) throws IllegalArgumentException {
        if (array.length != 8) {
            throw new IllegalArgumentException();
        }
        long value = 0;
        for (int i = 7; i >= 0; i--) {
            value = (value << 8);
            value |= (array[i] & 0x00FF);
        }

        return value;
    }

    /**
     * Assigns in the specified array the bytes of the specified long
     * primitive value in network order representation (big endian).
     * The most significant byte in the long value is stored at the lowest
     * index in the array.
     *
     * @param value the primitive long value.
     * @param array the byte array representing a long value in network order.
     * @throws IllegalArgumentException if the byte array size is incompatible with
     *                                  the long representation.
     */
    public static void longToByteArray(long value, byte[] array) {


        for (int i = 0; i < 8; i++) {
            array[i] = (byte) (value >> (8 * i));
        }

    }

    public static void main(String[] args) {
        long l = Long.parseLong(args[0]);

        byte[] array = new byte[8];

        longToByteArray(l, array);

        long l2 = byteArrayToLong(array);
        System.out.println((l == l2));
    }
}
